#!/bin/bash

# 生产有证书则略过
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout tt.key -out tt.crt -subj "/CN=*.tt.com/O=*.tt.com"

# secret tls 类型的  定义名字 tt-tls。 指定密钥和证书文件
kubectl create secret tls tt-tls --key tt.key --cert tt.crt -n ingress-nginx
